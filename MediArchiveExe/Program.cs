﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;
using mMediaArchive.FileInfoRetrievalAndCreation.Factories;
using mMediaArchive.RuntimeArguments;

namespace mMediaArchiveExe
{
    class Program
    {
        static IFileInfoGrouper _fileInfoGrouper;
        static void Main(string[] args)
        {
            if (SetPropertiesFromArgumentsAndDisplayHelpIfRequired(args))
                return;
           

            IEnumerable<string> inputFilenames = GetInputFilenames().ToList();
            
            var archiver = GetArchiver(inputFilenames);
            archiver.CreateDestinationFolders();

            archiver.ArchiveFiles(ArchiveMethodToUse, _fileInfoGrouper.GetYYYYMMFolderNamesToGroupingsMap());
        }

        /// <summary>
        /// returns 
        /// </summary>
        /// <param name="args"></param>
        /// <returns>true if help switch existed</returns>
        private static bool SetPropertiesFromArgumentsAndDisplayHelpIfRequired(string[] args)
         {
            //Focussing on AndroidImage files first, then AndroidVideo,then WP8Video, then WP8Image
            SetPropertiesFromArguments(args);
            if (HelpRequired)
                DisplayHelpRequiredMessage();
                
            return HelpRequired;
        }

        private static void DisplayHelpRequiredMessage()
        {
            Console.Write(Help.CommandSwitchUsage);
        }

        private static mMediaArchiver GetArchiver(IEnumerable<string> inputFilenames)
        {
            _fileInfoGrouper = GetFileInfoGrouper(inputFilenames);
            IFolderCreator folderCreator = new FolderCreator(OutputPath);
            IFileCopyOrMover fileCopyOrMover = new FileCopyOrMover(new DirectoryInfo(InputPath), new DirectoryInfo(OutputPath));
            var archiver = new mMediaArchiver(_fileInfoGrouper, folderCreator, fileCopyOrMover);
            
            return archiver;
        }

        private static IFileInfoGrouper GetFileInfoGrouper(IEnumerable<string> fileNames)
        {
            IFileInfoGrouper fileInfoGrouper = FileInfoGrouperFactory.GetGrouperFormMediaAType(MediaType, fileNames);
            return fileInfoGrouper;
        }

        private static IEnumerable<string> GetInputFilenames()
        {
            DirectoryInfo directory = new DirectoryInfo(InputPath);
            var files = directory.GetFiles();

            return files.Select(f => f.Name);
        }

        private static void SetPropertiesFromArguments(IEnumerable<string> args)
        {
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(args);
            HelpRequired = determinator.DoesHelpSwitchExist();
            if (HelpRequired)
                return;
            
            InputPath = determinator.GetInputPath();
            OutputPath = determinator.GetOutputPath();
            MediaType = determinator.GetMediaType();
            ArchiveMethodToUse = determinator.GetArchiveMethod();
            
        }

        protected static bool HelpRequired { get; set; }
        protected static ArchiveMethod ArchiveMethodToUse { get; set; }
        protected static mMediaAType MediaType { get; set; }
        protected static string OutputPath { get; set; }
        protected static string InputPath { get; set; }
    }
}

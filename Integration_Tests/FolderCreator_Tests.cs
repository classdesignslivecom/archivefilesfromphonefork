﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Integration_Tests;
using Interfaces;
using NUnit.Framework;
using mMediaArchive.FileInfoRetrievalAndCreation;

namespace FolderCreator_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void TakesStringFolderName_AsArgument()
        {
            string path = Environment.CurrentDirectory;
            FolderCreator creator = new FolderCreator(path);

            Assert.That(creator, Is.Not.Null);
        }
    }

    [TestFixture]
    public class CreateDestinationFolders
    {
        private TempDirectoryCreatorAndDeletor _tempDirectoryCreatorAndDeletor;

        [TestFixtureSetUp]
        public void Setup()
        {
            _tempDirectoryCreatorAndDeletor = new TempDirectoryCreatorAndDeletor(new DirectoryInfo(Environment.CurrentDirectory));

            _tempDirectoryCreatorAndDeletor.CreateTempDirectory();
        }

        [Test]
        public void CreatesTheFoldersSpecified_InTheArgumentList()
        {
            IList<string> foldersToCreate = GetFoldersToCreate().ToList();
            IFolderCreator folderCreator = new FolderCreator(_tempDirectoryCreatorAndDeletor.TempDirectory.FullName);
            folderCreator.CreateDestinationFolders(foldersToCreate);

            DirectoryInfo[] directoryListToCheck = _tempDirectoryCreatorAndDeletor.TempDirectory.GetDirectories();

            foreach (var directory in directoryListToCheck)
            {
                Assert.That(foldersToCreate.Contains(directory.Name), Is.True);              
            }
        }

        [TestFixtureTearDown]
        public void DeleteAllFoldersCreatedDuringTests()
        {
            _tempDirectoryCreatorAndDeletor.DeleteTempDirectoryAndSubDirectories();

            var tempDirectoryAndSubDirectories = _tempDirectoryCreatorAndDeletor.LocationForTempDirectory.GetDirectories();

            Assert.That(tempDirectoryAndSubDirectories.Count(), Is.EqualTo(0), GetAppRootTempFolderShouldNotExistMessage());
        }

        private IEnumerable<string> GetFoldersToCreate()
        {
            return new List<string>{"folder1", "folder2", "folder3", "folder4"};
        }

        private string GetAppRootTempFolderShouldNotExistMessage()
        {
            return "_appRootTemp(" + _tempDirectoryCreatorAndDeletor.TempDirectory.Name + ") should have been deleted by the teardown method";
        }
    }
}

﻿using System.IO;

namespace Integration_Tests
{
    public class TempDirectoryCreatorAndDeletor
    {
        private readonly DirectoryInfo _locationToCreateTempFolderIn;
        private DirectoryInfo _tempDirectory;
        private const string Temp = "Temp";

        public TempDirectoryCreatorAndDeletor(DirectoryInfo locationToCreateTempFolderIn)
        {
            _locationToCreateTempFolderIn = locationToCreateTempFolderIn;
        }

        public DirectoryInfo TempDirectory
        {
            get { return _tempDirectory; }
        }

        public DirectoryInfo LocationForTempDirectory {get { return _locationToCreateTempFolderIn; }}

        public void CreateTempDirectory()
        {           
            _locationToCreateTempFolderIn.CreateSubdirectory(Temp);

            _tempDirectory = new DirectoryInfo(_locationToCreateTempFolderIn.FullName + "\\" + Temp);
        }

        public void DeleteTempDirectoryAndSubDirectories()
        {
            _tempDirectory.Delete(true);
        }
    }
}
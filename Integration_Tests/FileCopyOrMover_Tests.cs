﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Integration_Tests;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers;
using NUnit.Framework;
using mMediaArchive.FileInfoRetrievalAndCreation;

namespace FileCopyOrMover_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void Takes2DirectoryInfos_SourceAndDestination()
        {
            IFileCopyOrMover filecopyOrMover = new FileCopyOrMover(new DirectoryInfo("source"), new DirectoryInfo("destination"));
            
            Assert.That(filecopyOrMover, Is.Not.Null);
        }
    }

    [TestFixture]
    public class Copy
    {
        #region Members
        private TempDirectoryCreatorAndDeletor _tempDirCreateDelete;
        private readonly string _sourceName = "Source";
        private readonly string _destinationName = "Destination";
        private readonly string _slash = "\\";
        private DirectoryInfo _sourceFolder;
        private DirectoryInfo _destinationRootFolder;
        private DirectoryInfo _destinationFolder201107;
        #endregion
        #region SetupAndTearDown
        [SetUp]
        public void Setup()
        {
           _tempDirCreateDelete = new TempDirectoryCreatorAndDeletor(new DirectoryInfo(Environment.CurrentDirectory));
            _tempDirCreateDelete.CreateTempDirectory();
            CreateTempSourceFolder();
            CreateTempDestinationFolder();
        }

        [TearDown]
        public void DeleteTempFiles()
        {
            _tempDirCreateDelete.DeleteTempDirectoryAndSubDirectories();
        }
        #endregion
        [Test]
        public void CopiesFilesFromSourceToDestination_WhenWeHave1GroupContaining5Files()
        {
            Create201107DirectoryAndInitialiseField();
            CreateTempSourceFiles(GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07());
            const int expectedNumberOfFiles5 = 5;
            List<string> expectedFileList = GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07();
            
            FileCopyOrMover copyer = new FileCopyOrMover(_sourceFolder, _destinationRootFolder);
            IEnumerable<IGrouping<string, string>> folderGroupsAndFileList = Get201107AndroidImageFileGrouper().GroupByYearAndMonth(); 
            copyer.Copy(folderGroupsAndFileList, Get201107AndroidImageFileGrouper().GetYYYYMMFolderNamesToGroupingsMap());
            IList<FileInfo> actualFileList = _destinationFolder201107.GetFiles();
           

            int actualNumberOfFiles = actualFileList.Count;
            Assert.That(actualNumberOfFiles, Is.EqualTo(expectedNumberOfFiles5));
            foreach (var file in actualFileList)
            {
                Assert.That(expectedFileList.Contains(file.Name), Is.True);
            }
        }

        [Test]
        public void CopiesFilesFromSourceToDestination_WhenWeHave2GroupsContaining10FilesHaving3DifferentDates()
        {
            Create201107DirectoryAndInitialiseField();
            Create201108Directory();
            CreateTempSourceFiles(GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07());
            CreateTempSourceFiles(GetListOfAndroidImageFilesFor201108With2DifferingDates_eg_WithYYYYMMPart2011Hypen08());
            const int expectedNumberOfFiles10 = 10;
            List<string> expectedFileList = GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07();
            expectedFileList.AddRange(GetListOfAndroidImageFilesFor201108With2DifferingDates_eg_WithYYYYMMPart2011Hypen08());

            FileCopyOrMover copyer = new FileCopyOrMover(_sourceFolder, _destinationRootFolder);

            IEnumerable<IGrouping<string, string>> folderGroupsAndFileList = Get201107And201108AndroidImageFileGrouper().GroupByYearAndMonth();
            copyer.Copy(folderGroupsAndFileList, Get201107And201108AndroidImageFileGrouper().GetYYYYMMFolderNamesToGroupingsMap());
            IList<FileInfo> actualFileList = _destinationRootFolder.EnumerateFiles("*", SearchOption.AllDirectories).ToList();


            int actualNumberOfFiles = actualFileList.Count;
            Assert.That(actualNumberOfFiles, Is.EqualTo(expectedNumberOfFiles10));
            foreach (var file in actualFileList)
            {
                Assert.That(expectedFileList.Contains(file.Name), Is.True);
            }
        }
        #region GetImageFiles
        private List<string> GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07()
        {
            List<string> files =  new List<string>();
            files.Add("2011-07-31 17.39.25.jpg");
            files.Add("2011-07-31 17.39.30.jpg");
            files.Add("2011-07-31 17.39.34.jpg");
            files.Add("2011-07-31 17.39.38.jpg");
            files.Add("2011-07-31 18.36.38.jpg");

            return files;
        }

        private List<string> GetListOfAndroidImageFilesFor201108With2DifferingDates_eg_WithYYYYMMPart2011Hypen08()
        {
            List<string> files = new List<string>();
            files.Add("2011-08-02 17.05.20.jpg");
            files.Add("2011-08-02 17.05.25.jpg");
            files.Add("2011-08-05 18.23.29.jpg");
            files.Add("2011-08-05 18.23.34.jpg");
            files.Add("2011-08-05 18.23.36.jpg");

            return files;
        }
        #endregion

        #region CreateFilesAndFolders
        private void CreateTempDestinationFolder()
        {
            CreateSubDirectoryOfTemp(_destinationName);
            _destinationRootFolder =
                new DirectoryInfo(_tempDirCreateDelete.TempDirectory.FullName + _slash + _destinationName);          
        }

        private void Create201107DirectoryAndInitialiseField()
        {
            IFileInfoGrouper infoGrouper = Get201107AndroidImageFileGrouper();
            CreateDirectoryFromFirstFolderInGroup(infoGrouper);

            _destinationFolder201107 = new DirectoryInfo(_destinationRootFolder.FullName + _slash + infoGrouper.GetFolderNamesFromGroups().First());
        }

        private void Create201108Directory()
        {
            IFileInfoGrouper infoGrouper = Get201108AndroidImageFileGrouper();
            CreateDirectoryFromFirstFolderInGroup(infoGrouper);
        }

        private void CreateDirectoryFromFirstFolderInGroup(IFileInfoGrouper grouperUsedToDetermineFolderName)
        {
            var dirToCreate = new DirectoryInfo(_destinationRootFolder.FullName + _slash + grouperUsedToDetermineFolderName.GetFolderNamesFromGroups().First());
            dirToCreate.Create();
        }

        private void CreateSubDirectoryOfTemp(string directoryName)
        {
            _tempDirCreateDelete.TempDirectory.CreateSubdirectory(directoryName);
        }

        private void CreateTempSourceFolder()
        {
            CreateSubDirectoryOfTemp(_sourceName);
            _sourceFolder = new DirectoryInfo(_tempDirCreateDelete.TempDirectory.FullName + _slash + _sourceName);
        }

        private void CreateTempSourceFiles(IList<string> files)
        {
            foreach (var file in files)
            {
                string fileIncludingPath = _sourceFolder.FullName + _slash + file;
                File.WriteAllText(fileIncludingPath, "stand back, nothingto see");
            }
        }
        #endregion

        #region GetAndroidImageFileGroupers
        private AndroidImageFileInfoGrouper Get201108AndroidImageFileGrouper()
        {
            return GetAndroidImageFileGrouperSpecified(GetListOfAndroidImageFilesFor201108With2DifferingDates_eg_WithYYYYMMPart2011Hypen08);
        }

        private AndroidImageFileInfoGrouper GetAndroidImageFileGrouperSpecified(Func<IEnumerable<string>> functionReturningFileList)
        {
            IFileMatchingFilter filter = new FileMatchingFilter(functionReturningFileList());
            return new AndroidImageFileInfoGrouper(filter);
        }

        private AndroidImageFileInfoGrouper Get201107AndroidImageFileGrouper()
        {
            return GetAndroidImageFileGrouperSpecified(GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07);
        }

        private AndroidImageFileInfoGrouper Get201107And201108AndroidImageFileGrouper()
        {
            List<string> filesFor201107And201108 = new List<string>();
            filesFor201107And201108.AddRange(GetListOfAndroidImageFilesFor201107_eg_WithYYYYMMformatHavingValues2011Hyphen07());
            filesFor201107And201108.AddRange(GetListOfAndroidImageFilesFor201108With2DifferingDates_eg_WithYYYYMMPart2011Hypen08());
            IFileMatchingFilter filter = new FileMatchingFilter(filesFor201107And201108);
            AndroidImageFileInfoGrouper grouper = new AndroidImageFileInfoGrouper(filter);

            return grouper;
        }
        #endregion
    }
}

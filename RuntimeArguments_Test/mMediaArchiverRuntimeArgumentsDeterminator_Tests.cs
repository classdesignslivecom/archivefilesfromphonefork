﻿using NUnit.Framework;
using RuntimeArguments_Test.Factories;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;
using mMediaArchive.RuntimeArguments;

namespace mMediaArchiverRuntimeArgumentsDeterminator_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void Takes_StringArray_AsArgument()
        {
            string[] args = {"test", "test"};

            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(args);

            Assert.That(determinator, Is.Not.Null);
        }
    }

    [TestFixture]
    public class GetInputPath
    {      
        [Test]
        public void Returns_String()
        {
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardAndroidArgs);

            var valueReturned = determinator.GetInputPath();

            Assert.That(valueReturned, Is.TypeOf(typeof (string)));
        }

        [Test]
        public void ReturnsTheString_AfterTheSlash_I()
        {
            string expectedString = RuntimeArgumentsFactory.InputPath;
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardAndroidArgs);

            string actualValueReturned = determinator.GetInputPath();

            Assert.That(actualValueReturned, Is.EqualTo(expectedString));
        }

        [Test, ExpectedException]
        public void ThrowsRequiredRuntimeArgumentMissingArgumentException_WhenSlashI_IsMissing()
        {
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.MissingSlashI);
            determinator.GetInputPath();
        }
    }

    [TestFixture]
    public class GetOutputPath
    {
        [Test]
        public void Returns_String()
        {
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardAndroidArgs);

            var valueReturned = determinator.GetOutputPath();

            Assert.That(valueReturned, Is.TypeOf<string>());
        }


        [Test]
        public void ReturnsTheStringAfter_SlashO()
        {
            string expectedString = RuntimeArgumentsFactory.OutputPath;
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardAndroidArgs);

            string actualValueReturned = determinator.GetOutputPath();

            Assert.That(actualValueReturned, Is.EqualTo(expectedString));
        }

        [Test, ExpectedException(typeof(RequiredRuntimeArgumentMissingException))]
        public void ThrowsRequiredRuntimeArgumentMissingException_WhenSlashO_IsMissing()
        {
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.MissingSlashO);
            determinator.GetOutputPath();
        }
    }

    [TestFixture]
    public class GetMediaType
    {
        [Test]
        public void Returns_TypeOf_mMediaAType()
        {
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardAndroidArgs);
            Assert.That(determinator.GetMediaType(), Is.TypeOf<mMediaAType>());
        }

        [Test]
        public void ReturnsMediaTypeAndroidImages_WhenStringAfterSlashM_IsAndroidImages()
        {
            const mMediaAType expectedMediaTypeOfAndroidImages = mMediaAType.AndroidImages;
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardAndroidArgs);
            
            var actualMediaTypeReturned = determinator.GetMediaType();
            
            Assert.That(actualMediaTypeReturned, Is.EqualTo(expectedMediaTypeOfAndroidImages));
        }

        [Test]
        public void ReturnsMediaTypeWP8Images_WhenStringAfterSlashM_IsWP8Images()
        {
            const mMediaAType expectedMediaTypeOfWP8 = mMediaAType.WP8Images;
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardWP8ImageArgsWithMove);
            
            var actualMediaTypeReturned = determinator.GetMediaType();
            
            Assert.That(actualMediaTypeReturned, Is.EqualTo(expectedMediaTypeOfWP8));
        }
    }

    [TestFixture]
    public class GetArchiveMethod
    {
        [Test]
        public void ReturnsArchiveMethodMove_WhenStringAfterSlashA_IsMove()
        {
            const ArchiveMethod expectedArchiveMethodOfMove = ArchiveMethod.Move;
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardWP8ImageArgsWithMove);

            ArchiveMethod actualArchiveMethodReturned = determinator.GetArchiveMethod();
           
            Assert.That(actualArchiveMethodReturned, Is.EqualTo(expectedArchiveMethodOfMove));
        }

        [Test]
        public void ReturnsArchiveMethodCopy_WhenStringAfterSlashA_IsCopy()
        {
            const ArchiveMethod expectedArchiveMethodOfCopy = ArchiveMethod.Copy;
            var determinator = new mMediaArchiverRuntimeArgumentsDeterminator(RuntimeArgumentsFactory.StandardWP8ImageArgsWithCopy);
            
            ArchiveMethod actualArchiveMethod = determinator.GetArchiveMethod();
           
            Assert.That(actualArchiveMethod, Is.EqualTo(expectedArchiveMethodOfCopy));
        }
    }
}

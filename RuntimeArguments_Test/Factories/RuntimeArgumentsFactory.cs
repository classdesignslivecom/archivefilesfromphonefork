namespace RuntimeArguments_Test.Factories
{
    public class RuntimeArgumentsFactory
    {
        private const string SlashI = "/i";
        private const string SlashO = "/o";
        private const string SlashM = "/m";
        private static string SlashA = "/a";
        private const string _inputPath = @"C:\test\input\path";
        private const string _outputPath = @"C:\test\output\path";
        private static readonly string[] SlashMAndroidImages = { SlashM, "AndroidImages" };
        private static readonly string[] SlashMWP8Images = {SlashM, "WP8Images"};
        private static string _archiveMethodOfMove = "move";
        private static string _archiveMethodOfCopy = "copy";

        //private static readonly string[] _baseArgs = { SlashI, _inputPath, SlashO, _outputPath};
        private static readonly string[] _baseArgsWithMove = { SlashI, _inputPath, SlashO, _outputPath, SlashA, _archiveMethodOfMove };
        private static readonly string[] _baseArgsWithCopy = { SlashI, _inputPath, SlashO, _outputPath, SlashA, _archiveMethodOfCopy };

        private static readonly string[] _missingSlashI = { _inputPath, SlashO, _outputPath };
        private static readonly string[] _missingSlashO = { SlashI, _inputPath, _outputPath };


        private static readonly string[] _standardAndroidArgs;
        private static string[] _standardWp8ImageArgsWithMove;
        private static string[] _standardWp8ImageArgsWithCopy;

        static RuntimeArgumentsFactory()
        {
            int baseArgsLength = _baseArgsWithMove.Length;
            int newArrayLength = baseArgsLength + SlashMAndroidImages.Length;
            _standardAndroidArgs = new string[newArrayLength];
            _standardWp8ImageArgsWithMove = new string[newArrayLength];
            _standardWp8ImageArgsWithCopy = new string[newArrayLength];
            _baseArgsWithMove.CopyTo(_standardAndroidArgs, 0);
            _baseArgsWithMove.CopyTo(_standardWp8ImageArgsWithMove, 0);
            _baseArgsWithCopy.CopyTo(_standardWp8ImageArgsWithCopy, 0);
            SlashMAndroidImages.CopyTo(_standardAndroidArgs, baseArgsLength);
            SlashMWP8Images.CopyTo(_standardWp8ImageArgsWithMove, baseArgsLength);;
        }

        public static string[] StandardAndroidArgs { get { return _standardAndroidArgs; } }

        public static string InputPath { get { return _inputPath; } }

        public static string OutputPath { get { return _outputPath; } }

        public static string[] MissingSlashI { get { return _missingSlashI; } }

        public static string[] MissingSlashO { get { return _missingSlashO; } }

        public static string[] StandardWP8ImageArgsWithMove { get { return _standardWp8ImageArgsWithMove; } }

        public static string[] StandardWP8ImageArgsWithCopy { get { return _standardWp8ImageArgsWithCopy; } }
    }
}
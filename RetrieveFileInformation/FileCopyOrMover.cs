﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Interfaces;

namespace mMediaArchive.FileInfoRetrievalAndCreation
{
    public class FileCopyOrMover : IFileCopyOrMover
    {
        private readonly DirectoryInfo _sourceLocation;
        private readonly DirectoryInfo _destinationLocation;
        private const string Slash = "\\";

        public FileCopyOrMover(DirectoryInfo sourceLocation, DirectoryInfo destinationLocation)
        {
            _sourceLocation = sourceLocation;
            _destinationLocation = destinationLocation;
        }

        /// <summary>
        /// Copies the files from _sourceLocation to _destinationLocation\GroupingKey\
        /// assuming that the "folders" (grouping Key) have already been created
        /// </summary>
        /// <param name="folderGroupsAndFiles"></param>
        public void Copy(IEnumerable<IGrouping<string, string>> folderGroupsAndFiles, IDictionary<string, string> groupsToYyyyMmFolderNameMap)
        {
            foreach (IGrouping<string, string> grouping in folderGroupsAndFiles)
            {
                string folder = groupsToYyyyMmFolderNameMap[grouping.Key];
                foreach (var file in grouping)
                {
                    string destFileName = _destinationLocation.FullName + Slash + folder + Slash + file;
                    if(File.Exists(destFileName))
                        continue;

                    string sourceFileName = _sourceLocation.FullName + Slash + file;
                    File.Copy(sourceFileName, destFileName, false);
                }
            }
        }

        public void Move(IEnumerable<IGrouping<string, string>> folderGroupsAndFileList, IDictionary<string, string> groupsToYyyyMmFolderNameMap)
        {
            throw new System.NotImplementedException();
        }
    }
}
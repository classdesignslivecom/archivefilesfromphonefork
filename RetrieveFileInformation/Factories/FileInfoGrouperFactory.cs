﻿using System;
using System.Collections.Generic;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;
using mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers;

namespace mMediaArchive.FileInfoRetrievalAndCreation.Factories
{
    public class FileInfoGrouperFactory
    {
        private static Dictionary<mMediaAType, Func<IEnumerable<string>, IFileInfoGrouper>> _fileInfoGroupersLookup;

       static FileInfoGrouperFactory()
       {
           SetupFileInfoGrouperLookup();
       }

        private static void SetupFileInfoGrouperLookup()
        {
            _fileInfoGroupersLookup = new Dictionary<mMediaAType, Func<IEnumerable<string>, IFileInfoGrouper>>();
            _fileInfoGroupersLookup.Add(mMediaAType.AndroidImages, GetAndroidImageFileGrouper);
            _fileInfoGroupersLookup.Add(mMediaAType.AndroidVideos, GetAndroidVideoFileGrouper);
        }

        private static IFileInfoGrouper GetAndroidVideoFileGrouper(IEnumerable<string> fileList)
        {
            return new AndroidVideoFileInfoGrouper(new FileMatchingFilter(fileList));
        }

        private static IFileInfoGrouper GetAndroidImageFileGrouper(IEnumerable<string> fileList)
        {
            return new AndroidImageFileInfoGrouper(new FileMatchingFilter(fileList));
        }

        public static IFileInfoGrouper GetGrouperFormMediaAType(mMediaAType mediaType, IEnumerable<string> fileList)
        {
            return _fileInfoGroupersLookup[mediaType].Invoke(fileList);
        }
    }
}

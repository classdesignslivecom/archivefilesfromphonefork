using System;
using System.Collections.Generic;
using System.Linq;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;

namespace mMediaArchive.FileInfoRetrievalAndCreation
{
// ReSharper disable InconsistentNaming
    public class mMediaArchiver
// ReSharper restore InconsistentNaming
    {
        private readonly IFileInfoGrouper _fileInfoGrouper;
        private readonly IFolderCreator _folderCreator;
        private readonly IFileCopyOrMover _fileCopyOrMover;
        private Dictionary<ArchiveMethod, Action<IEnumerable<IGrouping<string, string>>, IDictionary<string, string>>> _archiveMethodToFileCopyOrMoveFunctionMap;

        public mMediaArchiver(IFileInfoGrouper fileInfoGrouper, IFolderCreator folderCreator, IFileCopyOrMover fileCopyOrMover)
        {
            _fileInfoGrouper = fileInfoGrouper;
            _folderCreator = folderCreator;
            _fileCopyOrMover = fileCopyOrMover;
            SetupArchiveMethodToFileCopyOrMoveFunctionMap();
        }

        public void ArchiveFiles(ArchiveMethod archiveMethod, IDictionary<string, string> yyyymmFolderNamesToGroupingsMap)
        {
            _archiveMethodToFileCopyOrMoveFunctionMap[archiveMethod].Invoke(_fileInfoGrouper.GroupByYearAndMonth(), yyyymmFolderNamesToGroupingsMap);
        }

        public IEnumerable<string> GetListOfRequiredFolders()
        {
          return _fileInfoGrouper.GetFolderNamesFromGroups();
        }

        public void CreateDestinationFolders()
        {
            _folderCreator.CreateDestinationFolders(GetListOfRequiredFolders());
        }

        private void SetupArchiveMethodToFileCopyOrMoveFunctionMap()
        {
            _archiveMethodToFileCopyOrMoveFunctionMap =
                new Dictionary<ArchiveMethod, Action<IEnumerable<IGrouping<string, string>>, IDictionary<string, string>>>();

            _archiveMethodToFileCopyOrMoveFunctionMap.Add(ArchiveMethod.Copy, _fileCopyOrMover.Copy);
            _archiveMethodToFileCopyOrMoveFunctionMap.Add(ArchiveMethod.Move, _fileCopyOrMover.Move);
        }
    }
}
﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Interfaces;

namespace mMediaArchive.FileInfoRetrievalAndCreation
{
    public class FolderCreator :IFolderCreator
    {
        private readonly DirectoryInfo _myRootdirectory;

        public FolderCreator(string path)
        {
            _myRootdirectory = new DirectoryInfo(path);
        }

        public void CreateDestinationFolders(IEnumerable<string> foldersToCreate)
        {
            var subDirectoryInfosOfMyRoot = _myRootdirectory.GetDirectories().ToList();
            var subDirectoriesOfMyRoot = subDirectoryInfosOfMyRoot.Select(d => d.Name);

            var foldersNotYetExisting = foldersToCreate.Except(subDirectoriesOfMyRoot);

            foreach (var folder in foldersNotYetExisting)
            {
               _myRootdirectory.CreateSubdirectory(folder); 
            }
        }
    }
}
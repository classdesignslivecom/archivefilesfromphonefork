﻿namespace mMediaArchive.FileInfoRetrievalAndCreation.enums
{
    public enum mMediaAType
// ReSharper restore InconsistentNaming
    {
        UnSpecified = 0,
        AndroidImages,
        AndroidVideos,
        WP8Images
    }
}
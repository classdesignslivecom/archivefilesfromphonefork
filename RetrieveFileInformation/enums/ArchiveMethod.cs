﻿namespace mMediaArchive.FileInfoRetrievalAndCreation.enums
{
    public enum ArchiveMethod
    {
        Undefined = 0
        , Move
        , Copy
    }
}
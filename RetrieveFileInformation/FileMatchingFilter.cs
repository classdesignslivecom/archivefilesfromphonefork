﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using Interfaces;

namespace mMediaArchive.FileInfoRetrievalAndCreation
{
    public class FileMatchingFilter : IFileMatchingFilter
    {
        private readonly IEnumerable<string> _fileList;
        private const string AllFilesPattern = "*";

        public FileMatchingFilter(IEnumerable<string> fileList)
        {
            _fileList = fileList;
        }

        /// <summary>
        /// Gets all items in list based on a cases insensitive regex created
        /// using the string argument
        /// </summary>
        /// <param name="regexPatternFilter">value used to create a case insensitive regex. Must be pre-parsed 
        /// to not contain any path information </param>
        /// <returns>list of files matching the regex</returns>
        public IEnumerable<string> GetFilesMatchingFilter(string regexPatternFilter)
        {
            if (regexPatternFilter.Equals(AllFilesPattern))
                return _fileList;

            var fileFilterRegex = new Regex(regexPatternFilter, RegexOptions.IgnoreCase);
            var filesMatchingFilter = _fileList.Where(item => fileFilterRegex.IsMatch(item));

            return filesMatchingFilter;
        }
    }
}
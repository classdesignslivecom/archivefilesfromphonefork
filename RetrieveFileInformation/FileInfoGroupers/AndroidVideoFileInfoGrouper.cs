using System.Collections.Generic;
using System.Linq;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.Resources;

namespace mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers
{
    public class AndroidVideoFileInfoGrouper : IFileInfoGrouper
    {
        private readonly FileMatchingFilter _fileMatchingFilter;

        public AndroidVideoFileInfoGrouper(FileMatchingFilter fileMatchingFilter)
        {
            _fileMatchingFilter = fileMatchingFilter;
        }

        public IEnumerable<IGrouping<string, string>> GroupByYearAndMonth()
        {
            // e.g. the 2012-11 of video-2012-01-14-13-42-31.mp4
            return _fileMatchingFilter.GetFilesMatchingFilter(RegexFileFilters.AndroidVideoFiles)
                .GroupBy(fileName => fileName.Substring(6, 7));
        }

        public IEnumerable<string> GetFolderNamesFromGroups()
        {
            return GetYYYYMMFolderNamesToGroupingsMap().Select(yyyyMm => yyyyMm.Value);
        }

        public IDictionary<string, string> GetYYYYMMFolderNamesToGroupingsMap()
        {
            return GroupByYearAndMonth()
             .Select(g => g.Key)
             .ToDictionary(yyyyHyphenMM => yyyyHyphenMM, yyyyHyphenMM => yyyyHyphenMM.Replace("-", ""));
        }
    }
}
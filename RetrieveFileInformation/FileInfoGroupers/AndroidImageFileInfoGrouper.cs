using System.Collections.Generic;
using System.Linq;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.Resources;

namespace mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers
{
    public class AndroidImageFileInfoGrouper : IFileInfoGrouper
    {
        private readonly IFileMatchingFilter _fileMatchingFilter;

        public AndroidImageFileInfoGrouper(IFileMatchingFilter fileMatchingFilter)
        {
            _fileMatchingFilter = fileMatchingFilter;
        }

        public IEnumerable<IGrouping<string, string>> GroupByYearAndMonth()
        {
            return _fileMatchingFilter.GetFilesMatchingFilter(RegexFileFilters.AndroidImageFiles)
                .GroupBy(fileName => fileName.Substring(0, 7));
        }

        public IEnumerable<string> GetFolderNamesFromGroups()
        {
            return GetYYYYMMFolderNamesToGroupingsMap().Select(a => a.Value);
        }

        public IDictionary<string, string> GetYYYYMMFolderNamesToGroupingsMap()
        {
            return GroupByYearAndMonth()
                .Select(g => g.Key)
                .ToDictionary(yyyyHyphenMM => yyyyHyphenMM, yyyyHyphenMM => yyyyHyphenMM.Replace("-", ""));
        }
    }
}
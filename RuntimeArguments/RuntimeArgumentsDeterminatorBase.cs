﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace mMediaArchive.RuntimeArguments
{
    public class RuntimeArgumentsDeterminatorBase
    {
        public string GetArgsParameterValue(List<string> args, string switchValue)
        {
            int index = args.FindIndex(f => f.ToLower().Equals(switchValue.ToLower())) + 1;
            string parameterValue = args[index];

            return parameterValue;
        }

        public bool ArgsContainsSwitch(IEnumerable<string> args, string fileSwitch)
        {
            return args.Contains(fileSwitch, StringComparer.OrdinalIgnoreCase);
        }


        public bool DoesHelpSwitchExist(IEnumerable<string> args)
        {
            List<string> arguments = args.ToList();

            var slashHExists = arguments.Contains("/H", StringComparer.OrdinalIgnoreCase);

            return slashHExists;
        }
    }
}

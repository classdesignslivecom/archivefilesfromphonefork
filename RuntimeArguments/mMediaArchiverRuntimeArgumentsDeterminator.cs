﻿using System.Collections.Generic;
using System.Linq;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;

namespace mMediaArchive.RuntimeArguments
{
// ReSharper disable InconsistentNaming
    public class mMediaArchiverRuntimeArgumentsDeterminator
// ReSharper restore InconsistentNaming
    {
        private readonly List<string> _args;
        private readonly RuntimeArgumentsDeterminatorBase _determinator = new RuntimeArgumentsDeterminatorBase();
        private Dictionary<string, mMediaAType> _mMediaATypeLookup = new Dictionary<string, mMediaAType>();
        private Dictionary<string, ArchiveMethod> _archiveMethodLookup = new Dictionary<string, ArchiveMethod>();


        public mMediaArchiverRuntimeArgumentsDeterminator(IEnumerable<string> args)
        {
            _args = args.ToList();
            SetupmMediaATypeLookup();
            SetupArchiveMethodLookup();
        }

        public string GetInputPath()
        {
            const string inputPathSwitch = "/i";

            if (IsArgumentMissing(inputPathSwitch))
            {
                throw new RequiredRuntimeArgumentMissingException(inputPathSwitch);
            }
            
            return _determinator.GetArgsParameterValue(_args, inputPathSwitch);
        }

        private bool IsArgumentMissing(string argumentSwitchIdentifier)
        {
            return !_determinator.ArgsContainsSwitch(_args, argumentSwitchIdentifier);
        }

        public string GetOutputPath()
        {
            const string outputPathSwitch = "/o";
            if (IsArgumentMissing(outputPathSwitch))
            {
                throw new RequiredRuntimeArgumentMissingException(outputPathSwitch);
            }

            return _determinator.GetArgsParameterValue(_args, outputPathSwitch);
        }

        public mMediaAType GetMediaType()
        {
            const string mMediaTypeSwitch = "/m";
            var argsParameterValue = _determinator.GetArgsParameterValue(_args, mMediaTypeSwitch).ToLowerInvariant();
            
            return _mMediaATypeLookup[argsParameterValue];
        }

        public ArchiveMethod GetArchiveMethod()
        {
            const string archiveMethodSwitch = "/a";
            var argsParameterValue = _determinator.GetArgsParameterValue(_args, archiveMethodSwitch).ToLowerInvariant();

            return _archiveMethodLookup[argsParameterValue];
        }

        public bool DoesHelpSwitchExist()
        {
            return _determinator.DoesHelpSwitchExist(_args);
        }

        private void SetupArchiveMethodLookup()
        {
            _archiveMethodLookup.Add("move", ArchiveMethod.Move);
            _archiveMethodLookup.Add("copy",ArchiveMethod.Copy);
        }

        private void SetupmMediaATypeLookup()
        {
            _mMediaATypeLookup.Add("androidimages", mMediaAType.AndroidImages);
            _mMediaATypeLookup.Add("androidvideos", mMediaAType.AndroidVideos);
            _mMediaATypeLookup.Add("wp8images", mMediaAType.WP8Images);
        }
    }
}
﻿using System;

namespace mMediaArchive.RuntimeArguments
{
    public class RequiredRuntimeArgumentMissingException : ArgumentException
    {
        public RequiredRuntimeArgumentMissingException(string missingArgument) : base("Missing argument is " + missingArgument)
        {}
    }
}
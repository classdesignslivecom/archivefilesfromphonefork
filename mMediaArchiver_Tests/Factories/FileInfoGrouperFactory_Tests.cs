﻿using System.Collections.Generic;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation;
using mMediaArchive.FileInfoRetrievalAndCreation.Factories;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;
using mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers;
using NUnit.Framework;


namespace FileInfoGrouperFactory_Tests
{
    [TestFixture]
    public class GetGrouperFormMediaAType
    {
        [Test]
        public void ReturnsAndroidImageFileGrouper_WhenmMediaATypeAndroidImage_IsArgument()
        {
            IFileInfoGrouper actualGrouper = FileInfoGrouperFactory.GetGrouperFormMediaAType(mMediaAType.AndroidImages, new List<string>());
            
            Assert.That(actualGrouper, Is.InstanceOf<AndroidImageFileInfoGrouper>());
        }

        [Test]
        public void ReturnsAndroidVideoFileGrouper_WhenmMediaATypeAndroidVideos_IsArgument()
        {
            IFileInfoGrouper actualGrouper = FileInfoGrouperFactory.GetGrouperFormMediaAType(mMediaAType.AndroidVideos, new List<string>());
            
            Assert.That(actualGrouper, Is.TypeOf(typeof(AndroidVideoFileInfoGrouper)));
        }
    }
}

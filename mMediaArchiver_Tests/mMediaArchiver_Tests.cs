﻿using Interfaces;
using NSubstitute;
using NUnit.Framework;
using mMediaArchive.FileInfoRetrievalAndCreation;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;

namespace mMediaArchiver_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void Takes_IFileInfoGrouper_IFileMatchingFilter_IFolderCreatorAndIFileCopyOrMover_InConstructor()
        {
            IFileInfoGrouper fileInfoGrouper = Substitute.For<IFileInfoGrouper>();
            IFolderCreator folderCreator = Substitute.For<IFolderCreator>();
            IFileCopyOrMover fileCopyOrMover = Substitute.For<IFileCopyOrMover>();

            var archiver = new mMediaArchiver(fileInfoGrouper, folderCreator, fileCopyOrMover);
            
            Assert.That(archiver, Is.Not.Null);
        }
    }

    [TestFixture]
    public class GetListOfRequiredFolders
    {
        [Test]
        public void InvokesFileInfoGrouper_GetFolderNamesFromGroups()
        {
            IFileInfoGrouper fileInfoGrouper = Substitute.For<IFileInfoGrouper>();
            IFolderCreator folderCreator = Substitute.For<IFolderCreator>();
            IFileCopyOrMover fileCopyOrMover = Substitute.For<IFileCopyOrMover>();
            var archiver = new mMediaArchiver(fileInfoGrouper, folderCreator, fileCopyOrMover);

            archiver.GetListOfRequiredFolders();

            fileInfoGrouper.Received().GetFolderNamesFromGroups();
        }
    }

    [TestFixture]
    public class CreateDestinationFolders
    {
        [Test]
        public void InvokesFolderCreator_CreateDestinationFolders_WithItemsReturnedFromFileInfoGrouperGetFolderNamesFromGroups()
        {
            IFileInfoGrouper fileInfoGrouper = Substitute.For<IFileInfoGrouper>();
            IFolderCreator folderCreator = Substitute.For<IFolderCreator>();
            IFileCopyOrMover fileCopyOrMover = Substitute.For<IFileCopyOrMover>();
            var archiver = new mMediaArchiver(fileInfoGrouper, folderCreator, fileCopyOrMover);
            
            archiver.CreateDestinationFolders();

            folderCreator.Received().CreateDestinationFolders(fileInfoGrouper.GetFolderNamesFromGroups());       
        }
    }

    [TestFixture]
    public class ArchiveFiles
    {
        private IFileCopyOrMover _fileCopyOrMover;
        private IFolderCreator _folderCreator;
        private IFileInfoGrouper _fileInfoGrouper;
        
        [Test]
        public void InvokesFileCopyOrMover_CopyFilesWithGroupByYearAndMonth_WhenArchiveMethodIsCopy()
        {
            mMediaArchiver archiver = GetArchiver();

            archiver.ArchiveFiles(ArchiveMethod.Copy, null);

            _fileCopyOrMover.Received().Copy(_fileInfoGrouper.GroupByYearAndMonth(),null);
        }

        [Test]
        public void InvokesFileCopyOrMover_MoveFilesWithGroupByYearAndMonth_WhenArchiveMethodIsMove()
        {
            mMediaArchiver archiver = GetArchiver();

            archiver.ArchiveFiles(ArchiveMethod.Move, null);

            _fileCopyOrMover.Received().Move(_fileInfoGrouper.GroupByYearAndMonth(), null);
        }

        private mMediaArchiver GetArchiver()
        {
            _fileCopyOrMover = Substitute.For<IFileCopyOrMover>();
            _folderCreator = Substitute.For<IFolderCreator>();
            _fileInfoGrouper = Substitute.For<IFileInfoGrouper>();
            mMediaArchiver archiver = new mMediaArchiver(_fileInfoGrouper, _folderCreator, _fileCopyOrMover);

            return archiver;
        }
    }
}

﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IFolderCreator
    {
        void CreateDestinationFolders(IEnumerable<string> foldersToCreate);
    }
}
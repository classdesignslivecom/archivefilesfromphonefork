using System.Collections.Generic;
using System.Linq;

namespace Interfaces
{
    public interface IFileInfoGrouper
    {
        IEnumerable<IGrouping<string, string>> GroupByYearAndMonth();
        IEnumerable<string> GetFolderNamesFromGroups();
// ReSharper disable InconsistentNaming
        IDictionary<string, string> GetYYYYMMFolderNamesToGroupingsMap();
// ReSharper restore InconsistentNaming
    }
}
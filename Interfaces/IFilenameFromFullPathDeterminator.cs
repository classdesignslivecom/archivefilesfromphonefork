﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IFilenameFromFullPathDeterminator
    {
        IEnumerable<string> GetListOfAllFiles();
    }
}
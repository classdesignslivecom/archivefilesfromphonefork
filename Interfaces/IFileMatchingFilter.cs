﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IFileMatchingFilter
    {
        /// <summary>
        /// Gets all items in list based on a cases insensitive regex created
        /// using the string argument
        /// </summary>
        /// <param name="regexPatternFilter">value used to create a case insensitive regex. Must be pre-parsed 
        /// to not contain any path information </param>
        /// <returns>list of files matching the regex</returns>
        IEnumerable<string> GetFilesMatchingFilter(string regexPatternFilter);
    }
}
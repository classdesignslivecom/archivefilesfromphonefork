﻿using System.Collections.Generic;
using System.Linq;

namespace Interfaces
{
    public interface IFileCopyOrMover
    {
        void Copy(IEnumerable<IGrouping<string, string>> folderGroupsAndFiles, IDictionary<string,string> groupsToYyyyMmFolderNameMap);
        void Move(IEnumerable<IGrouping<string, string>> folderGroupsAndFileList, IDictionary<string,string> groupsToYyyyMmFolderNameMap);
    }
}

using System.Collections.Generic;
using FileInfoRetrievalAndCreation_Tests;
using FileMatchingFilter_Tests;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers;
using RetrieveFileInformation_Tests;
using mMediaArchive.FileInfoRetrievalAndCreation;

namespace FileInfoGrouper_Tests.Factories
{
    public class FileInfoGrouperFactoryForTests
    {
        public static IFileInfoGrouper GetAndroidImageFileInfoGrouper()
        {
            IEnumerable<string> fileList = GetAndroidImageFileList();
            IFileMatchingFilter filter = new FileMatchingFilter(fileList);
            var grouper = new AndroidImageFileInfoGrouper(filter);
            
            return grouper;
        }

        public static IFileInfoGrouper GetAndroidVideoFileInfoGrouper()
        {
            IEnumerable<string> fileList = GetAndroidVideoFiles();

            return new AndroidVideoFileInfoGrouper(new FileMatchingFilter(fileList));
        }

        public static IEnumerable<string> GetAndroidImageFileList()
        {
            return StubbedAndroidImagesFileLister.Get8ItemsContainingFilenamesWith2MonthsFromSameYear();
        }

        public static IEnumerable<string> GetAndroidVideoFiles()
        {
            return StubbedAndroidVideosFileLister.Get9ItemsContainingFilenamesWith2MonthsFromSameYear();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using FileInfoGrouper_Tests.Factories;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers;
using NUnit.Framework;
using RetrieveFileInformation_Tests;
using mMediaArchive.FileInfoRetrievalAndCreation;

namespace AndroidImageFileInfoGrouper_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void Takes_IFileMatchingFilter()
        {
            List<string> list = new List<string>();
            IFileMatchingFilter fileMatchingFilter = new FileMatchingFilter(list);
            
            AndroidImageFileInfoGrouper grouper = new AndroidImageFileInfoGrouper(fileMatchingFilter);
            
            Assert.That(grouper, Is.Not.Null);
        }
    }

    [TestFixture]
    public class GroupByYearAndMonth
    {
        [Test]
        public void Returns_IEnumerableOfType_IGroupingStringString()
        {
            IFileMatchingFilter filter = new FileMatchingFilter(new List<string>());
            IFileInfoGrouper grouper = new AndroidImageFileInfoGrouper(filter);
            
            IEnumerable<IGrouping<string, string>> returnedValue = grouper.GroupByYearAndMonth();
            
            Assert.That(returnedValue, Is.Not.Null);
        }

        [Test]
        public void ReturnsGroupContaining2Items_WhenInjectedListOfFiles_ContainsFilesFrom2Months()
        {
            int expectedNumberOfGroups = 2;
            IFileMatchingFilter filter = new FileMatchingFilter(StubbedAndroidImagesFileLister.Get8ItemsContainingFilenamesWith2MonthsFromSameYear());
            var grouper = new AndroidImageFileInfoGrouper(filter);
            
            var groupedItems = grouper.GroupByYearAndMonth();
            
            Assert.That(groupedItems.ToList().Count, Is.EqualTo(expectedNumberOfGroups));
        }

        [Test]
        public void FirstKeyInGroup_Is2011Hyphen07()
        {
            string expectedKeyOfFirstGroup = "2011-07";
            IFileMatchingFilter filter = new FileMatchingFilter(StubbedAndroidImagesFileLister.Get8ItemsContainingFilenamesWith2MonthsFromSameYear());
            var grouper = new AndroidImageFileInfoGrouper(filter);
            
            var actualKeyOfFirstGroup = grouper.GroupByYearAndMonth().First().Key;
            
            Assert.That(actualKeyOfFirstGroup, Is.EqualTo(expectedKeyOfFirstGroup));
        }

        [Test]
        public void FirstGroup_Contains2Items()
        {
            int expectedNumberOfItems = 2;
            IFileInfoGrouper grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();
            
            var actualNumberOfItems = grouper.GroupByYearAndMonth().First().Count();
            
            Assert.That(actualNumberOfItems, Is.EqualTo(expectedNumberOfItems));
        }

        [Test]
        public void FirstGroupsFirstItem_Is_2011Hyphen07Hyphen31Space17dot39dot38dotJpg()
        {
            string expectedFilename = "2011-07-31 17.39.38.jpg";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();
            
            string actual = grouper.GroupByYearAndMonth().First().First();
            
            Assert.That(actual.ToLower(), Is.EqualTo(expectedFilename.ToLower()));
        }

        [Test]
        public void FirstGroupsSecondItemIn_Is_2011Hyphen07Hyphen31Space18dot36dot38dotJpg()
        {
            string expectedFilename = "2011-07-31 18.36.38.jpg";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();

            string actual = grouper.GroupByYearAndMonth().First().Skip(1).First();

            Assert.That(actual.ToLower(), Is.EqualTo(expectedFilename.ToLower()));
        }

        [Test]
        public void SecondKeyInGroup_Is2011Hyphen08()
        {
            string expectedKeyOfSecondGroup = "2011-08";
            IEnumerable<string> fileList = FileInfoGrouperFactoryForTests.GetAndroidImageFileList();
            IFileMatchingFilter filter = new FileMatchingFilter(fileList);
            var grouper = new AndroidImageFileInfoGrouper(filter);

            var actualKeyOfFirstGroup = grouper.GroupByYearAndMonth().Skip(1).First().Key;

            Assert.That(actualKeyOfFirstGroup, Is.EqualTo(expectedKeyOfSecondGroup));
        }

        [Test]
        public void SecondGroup_Contains6Items()
        {
            int expectedNumberOfItems = 6;
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();

            var actualNumberOfItems = grouper.GroupByYearAndMonth().Skip(1).First().Count();

            Assert.That(actualNumberOfItems, Is.EqualTo(expectedNumberOfItems));
        }

        [Test]
        public void SecondGroupsFirstItem_Is_2011Hyphen08Hyphen02Space17dot05dot20dotJpg()
        {
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();
            string expectedFilename = "2011-08-02 17.05.20.jpg";
            
            string actual = grouper.GroupByYearAndMonth().Skip(1).First().First();

            Assert.That(actual.ToLower(), Is.EqualTo(expectedFilename.ToLower()));
        }

        [Test]
        public void SecondGroupsSixthItem_Is_2011Hyphen08Hyphen09Space13dot47dot59dotJpg()
        {
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();
            string expectedFilename = "2011-08-09 13.47.59.jpg";
            
            string actual = grouper.GroupByYearAndMonth().Skip(1)
                .First() //Second Group
                .Skip(5)
                .First();//6th Item in list

            Assert.That(actual.ToLower(), Is.EqualTo(expectedFilename.ToLower()));
        }
    }

    [TestFixture]
    public class GetYYYYMMFolderNamesToGroupingsMap
    {
        [Test]
        public void Returns_2Items_WhenListOfFilesComesFrom2DifferentMonths()
        {
            int expectedCount = 2;
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();
            
            int actualCount = grouper.GetYYYYMMFolderNamesToGroupingsMap().Count;
            
            Assert.That(actualCount, Is.EqualTo(expectedCount));
        }

        [Test]
        public void FirstYYYYMMKeyIs2011Hyphen07_HavingValue201107()
        {
            string expectedFirstKey = "2011-07";
            string expectedFirstValue = "201107";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();

            string actualFirstValue = grouper.GetYYYYMMFolderNamesToGroupingsMap().First().Value;
            string actualFirstKey = grouper.GetYYYYMMFolderNamesToGroupingsMap().First().Key;

            Assert.That(actualFirstValue, Is.EqualTo(expectedFirstValue));
            Assert.That(actualFirstKey, Is.EqualTo(expectedFirstKey));
        }

        [Test]
        public void SecondGroupName_Is_2011Hypen08()
        {
            string expectedSecondKey = "2011-08";
            string expectedSecondValue = "201108";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();

            string actualSecondValue = grouper.GetYYYYMMFolderNamesToGroupingsMap().Skip(1).First().Value;
            string actualSecondKey = grouper.GetYYYYMMFolderNamesToGroupingsMap().Skip(1).First().Key;

            Assert.That(actualSecondValue, Is.EqualTo(expectedSecondValue));
            Assert.That(actualSecondKey, Is.EqualTo(expectedSecondKey));
        }

    }

    [TestFixture]
    public class GetFolderNamesFromGroups
    {
        [Test]
        public void Returns201107And201108_When2011Hypen07And2011Hypen08AreTheGroupNames()
        {
            string expectedFirstValue = "201107";
            string expectedSecondValue = "201108";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidImageFileInfoGrouper();

            IEnumerable<string> folderNamesFromGroups = grouper.GetFolderNamesFromGroups();

            Assert.That(folderNamesFromGroups.Count(), Is.EqualTo(2));
            Assert.That(folderNamesFromGroups.Contains(expectedSecondValue), Is.True);
            Assert.That(folderNamesFromGroups.Contains(expectedFirstValue), Is.True);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using FileInfoGrouper_Tests.Factories;
using Interfaces;
using mMediaArchive.FileInfoRetrievalAndCreation;
using mMediaArchive.FileInfoRetrievalAndCreation.FileInfoGroupers;
using NUnit.Framework;

namespace AndroidVideoFileInfoGrouper_Tests
{
    [TestFixture]
    public class TheClass
    {
        [Test]
        public void Implements_IFileInfoGrouper()
        {
            var grouper = new AndroidVideoFileInfoGrouper(new FileMatchingFilter(new string[1]));
            
            Assert.That(grouper, Is.InstanceOf<IFileInfoGrouper>());
        }
    }

    [TestFixture]
    public class GroupByYearAndMonth
    {
        [Test]
        public void ReturnsGroupContaining2Items_WhenInjectedListOfFiles_ContainsFilesFrom2Months()
        {
            const int expectedNumberOfGroups2 = 2;
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();
            
            var actualNumberOfGroups = grouper.GroupByYearAndMonth().Count();
            
            Assert.That(actualNumberOfGroups, Is.EqualTo(expectedNumberOfGroups2));
        }

        [Test]
        public void FirstKeyInGroup_Is2012Hyphen01()
        {
            const string expectedKey2012Hyphen01 = "2012-01";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();
            
            var actualGroupKey = grouper.GroupByYearAndMonth().First().Key;
            
            Assert.That(actualGroupKey, Is.EqualTo(expectedKey2012Hyphen01));
        }

        [Test]
        public void FirstGroupContains4Items_WhenFileListContains4ItemsContaining2012Hyphen01InFilename()
        {
            const int  expectedNumberOf4 = 4;
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();
            
            int actualNumberOfItems = grouper.GroupByYearAndMonth().First().Count();
            
            Assert.That(actualNumberOfItems, Is.EqualTo(expectedNumberOf4));
        }

        [Test]
        public void SecondKeyInGroup_Is2012Hyphen02()
        {
            const string expectedKey2012Hyphen02 = "2012-02";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();

            var actualGroupKey = grouper.GroupByYearAndMonth().Skip(1).First().Key;

            Assert.That(actualGroupKey, Is.EqualTo(expectedKey2012Hyphen02));
        }
        [Test]
        public void SecondGroupContains5Items_WhenFileListContains5ItemsContaining2012Hyphen02InFilename()
        {
            const int expectedNumberOf5 = 5;
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();

            int actualNumberOfItems = grouper.GroupByYearAndMonth().Skip(1).First().Count();

            Assert.That(actualNumberOfItems, Is.EqualTo(expectedNumberOf5));
        }

        [Test]
        public void SecondGroupsFirstItem_Is_VideoHyphen2012Hypen02Hypen06Hypen14Hypen41Hypen11DotMp4()
        {
            const string expectedVideoHyphen2012Hypen02Hypen06Hypen14Hypen41Hypen11 = "video-2012-02-06-14-41-11.mp4";
            var  grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();
            
            var actualFileName = grouper.GroupByYearAndMonth().Skip(1).First().First();
            
            Assert.That(actualFileName, Is.EqualTo(expectedVideoHyphen2012Hypen02Hypen06Hypen14Hypen41Hypen11));
        }
    }

    [TestFixture]
    public class GetYYYYMMFolderNamesToGroupingsMap
    {
        [Test]
        public void Returns_2Items_WhenListOfFilesComesFrom2DifferentMonths()
        {
            int expectedCount = 2;
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();

            int actualCount = grouper.GetYYYYMMFolderNamesToGroupingsMap().Count;

            Assert.That(actualCount, Is.EqualTo(expectedCount));
        }

        [Test]
        public void FirstYYYYMMKeyIs2012Hyphen01_HavingValue201107()
        {
            string expectedFirstKey = "2012-01";
            string expectedFirstValue = "201201";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();

            string actualFirstValue = grouper.GetYYYYMMFolderNamesToGroupingsMap().First().Value;
            string actualFirstKey = grouper.GetYYYYMMFolderNamesToGroupingsMap().First().Key;

            Assert.That(actualFirstValue, Is.EqualTo(expectedFirstValue));
            Assert.That(actualFirstKey, Is.EqualTo(expectedFirstKey));
        }

        [Test]
        public void SecondGroupName_Is_2012Hypen02()
        {
            string expectedSecondKey = "2012-02";
            string expectedSecondValue = "201202";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();

            string actualSecondValue = grouper.GetYYYYMMFolderNamesToGroupingsMap().Skip(1).First().Value;
            string actualSecondKey = grouper.GetYYYYMMFolderNamesToGroupingsMap().Skip(1).First().Key;

            Assert.That(actualSecondValue, Is.EqualTo(expectedSecondValue));
            Assert.That(actualSecondKey, Is.EqualTo(expectedSecondKey));
        }
    }

    [TestFixture]
    public class GetFolderNamesFromGroups
    {
        [Test]
        public void Returns201201And201202_When2012Hypen01And2012Hypen02AreTheGroupNames()
        {
            string expectedFirstValue = "201201";
            string expectedSecondValue = "201202";
            var grouper = FileInfoGrouperFactoryForTests.GetAndroidVideoFileInfoGrouper();

            IEnumerable<string> folderNamesFromGroups = grouper.GetFolderNamesFromGroups();

            Assert.That(folderNamesFromGroups.Count(), Is.EqualTo(2));
            Assert.That(folderNamesFromGroups.Contains(expectedSecondValue), Is.True);
            Assert.That(folderNamesFromGroups.Contains(expectedFirstValue), Is.True);
        }
    }
}

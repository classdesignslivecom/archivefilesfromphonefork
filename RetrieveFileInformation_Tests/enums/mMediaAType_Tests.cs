﻿using System;
using mMediaArchive.FileInfoRetrievalAndCreation.enums;
using NUnit.Framework;

namespace FileInfoRetrievalAndCreation_Tests.enums
{
    [TestFixture]
    public class EnumValueUndefined
    {
        [Test]
        public void HasValueOf_0()
        {
            string expectedNameOfValueHavingZero = "UnSpecified";
            
            string nameHavingValueOfZero = Enum.GetName(typeof(mMediaAType), 0);
            
            Assert.That(nameHavingValueOfZero, Is.EqualTo(expectedNameOfValueHavingZero));
        }
    }

    [TestFixture]
    public class EnumValueAndroidImages
    {
        [Test]
        public void Exists()
        {
            const string stringValueOfAndroidImages = "AndroidImages";
            
            int intValueOfAndroidImages = (int)Enum.Parse(typeof(mMediaAType), stringValueOfAndroidImages);

            Assert.That(intValueOfAndroidImages, Is.GreaterThan(0));
        }
    }

    [TestFixture]
    public class EnumValueAndroidVideos
    {
        [Test]
        public void Exists()
        {
            const string stringValueOfAndroidVideos = "AndroidVideos";

            int intValueOfAndroidVideos = (int)Enum.Parse(typeof(mMediaAType), stringValueOfAndroidVideos);
              
            Assert.That(intValueOfAndroidVideos, Is.GreaterThan(0));
        }
    }

    [TestFixture]
    public class EnumValueWP8Images
    {
        [Test]
        public void Exists()
        {
            const string stringValueOfWP8Images = "WP8Images";
            
            int intValueOfWP8 = (int)Enum.Parse(typeof(mMediaAType), stringValueOfWP8Images);

            Assert.That(intValueOfWP8, Is.GreaterThan(0));
        }
    }

}

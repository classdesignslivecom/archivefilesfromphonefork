﻿using System.Collections.Generic;
using mMediaArchive.FileInfoRetrievalAndCreation;

namespace RetrieveFileInformation_Tests
{
    public class StubbedWP8FileLister
    {
        public static IEnumerable<string> GetVideoFilenamesFrom2YearsAnd2Months()
        {
            var filesFrom2YearsAnd2Months = new List<string>
                {
                    "video-2012-12-09-20-43-09.mp4",
                    "video-2012-12-28-10-56-57(SleepingAndSmiling).mp4",
                    "video-2012-12-28-10-58-16.mp4",
                    "video-2012-12-28-11-01-47.mp4",
                    "video-2012-12-28-11-52-20.mp4",
                    "video-2012-12-28-13-04-09.mp4",
                    "video-2012-12-28-18-38-13.mp4",
                    "video-2012-12-28-20-39-55.mp4",
                    "video-2012-12-30-11-22-27.mp4",
                    "video-2012-12-30-11-26-09.mp4",
                    "video-2012-12-30-12-59-50.mp4",
                    "video-2012-12-30-13-00-54.mp4",
                    "video-2012-12-30-19-24-18.mp4",
                    "video-2013-01-01-11-10-10.mp4",
                    "video-2013-01-05-13-02-41.mp4",
                    "video-2013-01-06-11-10-54.mp4",
                    "video-2013-01-06-14-45-16.mp4",
                    "video-2013-01-06-18-19-24.mp4",
                    "video-2013-01-08-18-09-51.mp4",
                    "video-2013-01-08-18-10-16.mp4",
                    "video-2013-01-10-18-21-29.mp4",
                    "video-2013-01-10-18-23-06.mp4",
                    "video-2013-01-10-18-28-46.mp4",
                    "video-2013-01-10-19-41-31.mp4",
                    "video-2013-01-11-18-43-01.mp4",
                    "video-2013-01-11-18-43-08.mp4",
                    "video-2013-01-13-16-07-33.mp4",
                    "video-2013-01-13-18-57-36.mp4",
                    "video-2013-01-13-19-04-19.mp4",
                    "video-2013-01-13-19-05-47.mp4",
                    "video-2013-01-13-19-16-02.mp4",
                    "video-2013-01-13-19-19-25.mp4",
                    "video-2013-01-17-19-18-19.mp4",
                    "video-2013-01-19-15-28-43.mp4",
                    "video-2013-01-19-18-52-13.mp4",
                    "video-2013-01-20-11-53-13.mp4",
                    "video-2013-01-21-18-06-14.mp4",
                    "video-2013-01-27-09-19-30.mp4",
                    "video-2013-01-30-18-29-13.mp4"
                };

            return filesFrom2YearsAnd2Months;
        }

        /* public static IEnumerable<string> GetFullPathAndFilenamesFrom2YearsAnd2Months()
        {
            var filesFrom2YearsAnd2Months = new List<string>
                {
                    @"D:\PhoneWP8Transfer\video-2012-12-09-20-43-09.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-10-56-57(SleepingAndSmiling).mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-10-58-16.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-11-01-47.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-11-52-20.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-13-04-09.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-18-38-13.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-28-20-39-55.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-30-11-22-27.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-30-11-26-09.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-30-12-59-50.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-30-13-00-54.mp4",
                    @"D:\PhoneWP8Transfer\video-2012-12-30-19-24-18.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-01-11-10-10.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-05-13-02-41.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-06-11-10-54.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-06-14-45-16.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-06-18-19-24.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-08-18-09-51.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-08-18-10-16.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-10-18-21-29.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-10-18-23-06.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-10-18-28-46.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-10-19-41-31.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-11-18-43-01.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-11-18-43-08.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-13-16-07-33.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-13-18-57-36.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-13-19-04-19.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-13-19-05-47.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-13-19-16-02.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-13-19-19-25.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-17-19-18-19.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-19-15-28-43.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-19-18-52-13.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-20-11-53-13.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-21-18-06-14.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-27-09-19-30.mp4",
                    @"D:\PhoneWP8Transfer\video-2013-01-30-18-29-13.mp4"
                };

            return filesFrom2YearsAnd2Months;
        }*/
    }
}

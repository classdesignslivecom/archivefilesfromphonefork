﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileInfoRetrievalAndCreation_Tests
{
    public class StubbedAndroidVideosFileLister
    {
        public static IEnumerable<string> Get9ItemsContainingFilenamesWith2MonthsFromSameYear()
        {
            var twoMonthsFromSameYear = new List<string>
            {
                "video-2012-01-14-13-42-31.mp4",
                "video-2012-01-14-15-38-20.mp4",
                "video-2012-01-14-15-38-45.mp4",
                "video-2012-01-15-17-13-27.mp4",
                "video-2012-02-06-14-41-11.mp4",
                "video-2012-02-06-14-41-49.mp4",
                "video-2012-02-09-10-08-53.mp4",
                "video-2012-02-09-10-09-03.mp4",
                "video-2012-02-10-17-13-49.mp4"
            };

            return twoMonthsFromSameYear;

        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RetrieveFileInformation_Tests;
using mMediaArchive.FileInfoRetrievalAndCreation;
using mMediaArchive.FileInfoRetrievalAndCreation.Resources;

namespace FileMatchingFilter_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void TakesArgumentOf_IEnumerableString()
        {
            var fileMatcher =
                new FileMatchingFilter(
                    StubbedAndroidImagesFileLister.Get8ItemsContainingFilenamesWith2MonthsFromSameYear());

            Assert.That(fileMatcher, Is.Not.Null);
        }
    }

    [TestFixture]
    public class GetAndroidImageFiles
    {
        private readonly string _androidImageFilter = RegexFileFilters.AndroidImageFiles;


        [Test]
        public void FirstFourCharacters_OfReturnedFiles_AreDigits()
        {
            var filteredListOfFiles = GetFileListFilteredByAndroidImageFilter();

            foreach (string fileName in filteredListOfFiles)
            {
                Assert.That((int.Parse(fileName.Substring(0, 4))), Is.TypeOf(typeof(int)));
            }
        }

        [Test]
        public void FifthCharacter_OfReturnedFiles_IsHyphen()
        {
            var filteredListOfFiles = GetFileListFilteredByAndroidImageFilter();

            foreach (string fileName in filteredListOfFiles)
            {
                AssertThatCharXIsHyphen(fileName, 4);
            }
        }

        [Test]
        public void SixthAndSeventhCharacters_OfReturnedFiles_AreDigits()
        {
            var filteredListOfFiles = GetFileListFilteredByAndroidImageFilter();

            foreach (string fileName in filteredListOfFiles)
            {
                Assert.That((int.Parse(fileName.Substring(5, 2))), Is.TypeOf(typeof(int)));
            }
        }

        [Test]
        public void EighthCharacter_OfReturnedFiles_IsHyphen()
        {
            var filteredListOfFiles = GetFileListFilteredByAndroidImageFilter();
        
            foreach (string fileName in filteredListOfFiles)
            {
                AssertThatCharXIsHyphen(fileName, 7);
            }
        }

        private static void AssertThatCharXIsHyphen(string fileName, int expectedZeroBasedIndexOfHyphen)
        {
            Assert.That(fileName.ElementAt(expectedZeroBasedIndexOfHyphen), Is.EqualTo('-'));
        }


        private IEnumerable<string> GetFileListFilteredByAndroidImageFilter()
        {
            FileMatchingFilter filter = GetFilterContainingAllAndroidFilesWithoutPath();
            var filteredListOfFiles = filter.GetFilesMatchingFilter(_androidImageFilter);

            return filteredListOfFiles;
        }


        private FileMatchingFilter GetFilterContainingAllAndroidFilesWithoutPath()
        {
            return new FileMatchingFilter(StubbedAndroidImagesFileLister.GetAllAndroidPhotoFiles());
        }
    }

    [TestFixture]
    public class GetFilesMatchingFilter
    {
        [Test]
        public void PatternOf_Asterisk_ReturnsAllFilesInConstructorArgument()
        {
            const int expectedNumberOfItems8 = 8;
            var filter = GetFilterContainingAllAndroidFilesWithFullPath();

            int numberOfItemsInReturnedFileList = filter.GetFilesMatchingFilter("*").Count();

            Assert.That(numberOfItemsInReturnedFileList, Is.EqualTo(expectedNumberOfItems8));
        }

        [Test]
        public void PatternOf_VideoAsterisk_ReturnsNoAndroidVideoFiles()
        {
            const int expectedCountOf0 = 0;
            FileMatchingFilter filter = GetFilterContainingAllAndroidVideoFilesWithFullPath();

            int actualCount = filter.GetFilesMatchingFilter(RegexFileFilters.WP8Video).Count();

            Assert.That(actualCount, Is.EqualTo(expectedCountOf0));
        }

        [Test]
        public void PatternOf_VideoAsteriskDotmp4_ReturnsAll39WP8VideoFiles()
        {
            const int expectedCountOf39 = 39;
            FileMatchingFilter filter = GetFilterContainingAllWP8VideoFilesWithoutFullPath();

            string regexPatternFilter = RegexFileFilters.WP8Video;
            int actualCount = filter.GetFilesMatchingFilter(regexPatternFilter).Count();

            Assert.That(actualCount, Is.EqualTo(expectedCountOf39));
        }

        [Test]
        public void PatterOf_VLideoAsterisk_ReturnsNoWP8VideoFiles()
        {
            const int expectedCountOf0 = 0;
            FileMatchingFilter filter = GetFilterContainingAllWP8VideoFilesWithoutFullPath();

            int actualCount = filter.GetFilesMatchingFilter("VLideo*").Count();

            Assert.That(actualCount, Is.EqualTo(expectedCountOf0));
        }

        private FileMatchingFilter GetFilterContainingAllAndroidVideoFilesWithFullPath()
        {
            return new FileMatchingFilter(StubbedAndroidImagesFileLister.GetAllAndroidVideoFiles());
        }

        private static FileMatchingFilter GetFilterContainingAllAndroidFilesWithFullPath()
        {
            return new FileMatchingFilter(StubbedAndroidImagesFileLister.Get8ItemsContainingFilenamesWith2MonthsFromSameYear());
        }

        private FileMatchingFilter GetFilterContainingAllWP8VideoFilesWithoutFullPath()
        {
            return new FileMatchingFilter(StubbedWP8FileLister.GetVideoFilenamesFrom2YearsAnd2Months());
        }
    }
}